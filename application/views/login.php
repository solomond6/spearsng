<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Spear Ng</title>
	<?php echo link_tag('assets/css/bootstrap.min.css'); ?>
	<?php echo link_tag('assets/css/jquery-ui.css'); ?>
	<?php echo link_tag('assets/css/jquery-ui.theme.css'); ?>
	<?php echo link_tag('assets/css/style.css'); ?>
</head>
<body>
	<div class="container mt-5 pt-5">
		<div class="row justify-content-center">
			<div id="body" class="col-4 mt-4 pt-4">
				<?php echo '<alert class="alert alert-danger">'.validation_errors().'</alert>'; if(isset($_SESSION['message'])){echo '<alert class="alert alert-danger">'.$_SESSION['message'].'</alert>';}?>
				<br/>
				<h2 class="text-center">Login Form</h2>
				<?php echo form_open('login/validate_credentials'); ?>
	          	<div class="form-group">
		            <label for="recipient-name" class="col-form-label text-left">Email Address:</label>
		            <input type="text" class="form-control" name="username" required="required">
		        </div>
		        <div class="form-group">
		            <label for="message-text" class="col-form-label text-left">Password:</label>
		            <input type="password" class="form-control" name="password" required="required">
		        </div>
			    <div class="form-group">
			        <button type="submit" class="btn btn-primary">Login</button>
			    </div>
		        <?php echo form_close(); ?>
			</div>
		</div>
	</div>
</body>
<script src="<?php echo base_url();?>assets/js/jquery.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
</html>