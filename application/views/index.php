<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Spear Ng</title>
	<?php echo link_tag('assets/css/bootstrap.min.css'); ?>
	<?php echo link_tag('assets/css/jquery-ui.css'); ?>
	<?php echo link_tag('assets/css/jquery-ui.theme.css'); ?>
	<?php echo link_tag('assets/css/style.css'); ?>
</head>
<body>
<div class="container-fluid mt-1">
	<div class="pull-right text-right">
		<button type="button" class="btn btn-default" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Add Article</button>
	</div>
</div>
<div class="container mt-5 pt-5 text-center">
	<div class="row justify-content-center">
		<div id="body" class="col-8 mt-4 pt-4">
			<?php echo '<alert class="alert alert-danger">'.validation_errors().'</alert>'; if(isset($_SESSION['message'])){echo '<alert class="alert alert-danger">'.$_SESSION['message'].'</alert>';}?>
			<br/>
			<h1>Stears Ng</h1>
			<div class="form-group">
				<input type="text" name="search" id="search" class="form-control">
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      	<div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">New Article</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	    </div>
        <?php echo form_open('home/addarticles'); ?>
      	<div class="modal-body">
          	<div class="form-group">
	            <label for="recipient-name" class="col-form-label">Title:</label>
	            <input type="text" class="form-control" name="title" required="required">
	        </div>
	        <div class="form-group">
	            <label for="message-text" class="col-form-label">Content:</label>
	            <textarea class="form-control" id="message-text" name="contents" rows="8" required="required"></textarea>
	        </div>
	    </div>
	    <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Submit</button>
	    </div>
        <?php echo form_close(); ?>
    </div>
  </div>
</div>

</body>
<script src="<?php echo base_url();?>assets/js/jquery.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script>
    $("#search").autocomplete({
      	minLength: 2,
      	source: function(request,response) {
          	$.ajax({
	            url: "<?php echo site_url('home/search'); ?>",
	            type: "POST",
	            dataType: "json",
	            data: { search: request.term },
	            success: function (data) {
	                response($.map(data, function (item) {
	                    return { label: item.title, value: item.title};
	                }))
	            }
          	})
      	},
      	focus : function(){ return false; }
  	});
</script>
</html>