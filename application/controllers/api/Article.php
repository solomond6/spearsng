<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Article extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        
        $this->load->model('Article_model');
        $this->methods['articles_post']['limit'] = 100; // 100 requests per hour per user/key
    }

    public function articles_post()
    {
        $query = $this->Article_model->addArticle()

        if($query)
        {
            $message = [
                'status' => 'Ok',
                'message' => 'Article Posted'
            ];
            $this->response([$message], REST_Controller::HTTP_OK);
        }else{
            $message = [
                'status' => '',
                'message' => 'Error Occured'
            ];
            $this->response([$message], REST_Controller::HTTP_NOT_FOUND);
        }
    }

}
