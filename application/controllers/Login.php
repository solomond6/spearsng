<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");

class Login extends CI_Controller {

	public function index()
	{
		$this->load->view('login');
	}

	public function validate_credentials(){
		
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
    	$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if($this->form_validation->run() == FALSE)
		{
			$this->load->view('login');
		}else{
			$this->load->model('Users_model');
			$query = $this->Users_model->getUser();
			if($query)
			{
				$data = array(
					'username'=> $this->input->post('username'),
                    'is_logged_in' => true
				);
				$this->session->set_userdata($data);
	            $this->session->set_flashdata('message', 'Login Successfully.');
					            redirect('home/index');
			}else{
	            //$this->message->set('message', 'Incorrect Username/Password.');
	            $this->session->set_flashdata('message', 'Incorrect Username/Password.');
	            $this->index();
			}
		}
	}
}