<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");

class Home extends CI_Controller {

	public function index()
	{
		$this->load->view('index');
	}

	public function search(){
		$search = $_POST['search'];
		$this->db->select('*');
        $this->db->like('title', $search);
        $data = $this->db->get('articles')->result_array();
        echo json_encode($data);
        exit;
	}

	public function addarticles(){
		
		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		$this->form_validation->set_rules('contents', 'Body', 'trim|required');
		
		if($this->form_validation->run() == FALSE){
			$data['page_title']  = "Add Article";
			$this->load->view('index', $data);
		}else{	
			$this->load->model('Article_model');
			if($query = $this->Article_model->addArticle()){
				$this->session->set_flashdata('message', 'Article Created Successfully');
				redirect('home/index');
			}else{
				redirect('home/index');
			}
		}
	}

	public function createUser($to = 'World'){
		//echo "Hello {$to}!".PHP_EOL;
		echo "Enter your email address to continue: ";
		$handle = fopen ("php://stdin","r");
		$email = fgets($handle);
		if(trim($email) != ''){
		   	echo "Enter your password to continue: ";
			$handle2 = fopen ("php://stdin","r");
			$password = fgets($handle2);
			fclose($handle2);
			echo "\n";
		}else{
			fclose($handle);
			echo "\n";
			echo "Please provide an email address".PHP_EOL;
		}
		fclose($handle);
		
		$insert_new_user = array(
			'email'=> trim($email),
			'password'=> trim($password)
		);
	
		$insert= $this->db->insert('users', $insert_new_user);
		if($insert){
			echo "User created successfully".PHP_EOL;
		}else{
			echo "There was an error creating user, Try again".PHP_EOL;
		}

	}
	
}