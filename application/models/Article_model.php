<?php

Class Article_model extends CI_Model{
	
	function addArticle() {
		
		$insert_new_article = array(
			'title'=> $this->input->post('title'),
			'content'=> $this->input->post('contents')
		);
	
		$insert= $this->db->insert('articles', $insert_new_article);
		return $insert;
	}
}