-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 28, 2018 at 04:34 AM
-- Server version: 5.6.17
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spearng`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `title`, `content`, `created_at`) VALUES
(1, 'New Article Tins', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque pharetra nec urna at convallis. Nulla interdum ornare quam, ut ultrices nibh fermentum ac. Vivamus vel magna sit amet erat interdum maximus eget vel dolor. Duis ut consequat ante, ut congue odio. Curabitur vitae ultrices nibh. Fusce varius elit leo, vitae gravida mi condimentum et. Mauris tellus lacus, pharetra vitae turpis tempor, auctor feugiat nibh. Nunc molestie mattis ultricies. Ut quam augue, ultrices sed odio sit amet, condimentum condimentum ante.\r\n\r\nSuspendisse dapibus ex nisl, ac ultrices nulla accumsan quis. Nam consectetur in diam id pellentesque. Ut feugiat eleifend dictum. Pellentesque pretium, lectus in consequat sollicitudin, diam tellus hendrerit mi, eget pretium lorem augue quis dui. Sed nec ante sem. Pellentesque at consectetur lorem. Quisque eleifend felis non orci gravida dignissim. Donec malesuada mi et elit maximus, nec vestibulum lacus scelerisque. Ut faucibus molestie eros, non venenatis ex luctus vitae. Vivamus cursus eros eu velit hendrerit auctor. In augue dui, ultrices sit amet magna sed, luctus facilisis augue. Donec id sapien magna.\r\n\r\nMorbi arcu nulla, venenatis at ex nec, dignissim commodo arcu. Ut dictum orci at dui vestibulum ultricies. Fusce vestibulum aliquam lacus vitae fringilla. Nulla sagittis a tortor nec tristique. Curabitur at lacus sed justo porta mollis. Etiam iaculis et sem eget ornare. Maecenas sed condimentum turpis, quis volutpat leo. Vivamus quis nulla est.', '2018-06-27 12:15:59'),
(2, 'New Article Tins', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque pharetra nec urna at convallis. Nulla interdum ornare quam, ut ultrices nibh fermentum ac. Vivamus vel magna sit amet erat interdum maximus eget vel dolor. Duis ut consequat ante, ut congue odio. Curabitur vitae ultrices nibh. Fusce varius elit leo, vitae gravida mi condimentum et. Mauris tellus lacus, pharetra vitae turpis tempor, auctor feugiat nibh. Nunc molestie mattis ultricies. Ut quam augue, ultrices sed odio sit amet, condimentum condimentum ante.\r\n\r\nSuspendisse dapibus ex nisl, ac ultrices nulla accumsan quis. Nam consectetur in diam id pellentesque. Ut feugiat eleifend dictum. Pellentesque pretium, lectus in consequat sollicitudin, diam tellus hendrerit mi, eget pretium lorem augue quis dui. Sed nec ante sem. Pellentesque at consectetur lorem. Quisque eleifend felis non orci gravida dignissim. Donec malesuada mi et elit maximus, nec vestibulum lacus scelerisque. Ut faucibus molestie eros, non venenatis ex luctus vitae. Vivamus cursus eros eu velit hendrerit auctor. In augue dui, ultrices sit amet magna sed, luctus facilisis augue. Donec id sapien magna.\r\n\r\nMorbi arcu nulla, venenatis at ex nec, dignissim commodo arcu. Ut dictum orci at dui vestibulum ultricies. Fusce vestibulum aliquam lacus vitae fringilla. Nulla sagittis a tortor nec tristique. Curabitur at lacus sed justo porta mollis. Etiam iaculis et sem eget ornare. Maecenas sed condimentum turpis, quis volutpat leo. Vivamus quis nulla est.', '2018-06-27 12:16:37'),
(3, 'Second Article', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque pharetra nec urna at convallis. Nulla interdum ornare quam, ut ultrices nibh fermentum ac. Vivamus vel magna sit amet erat interdum maximus eget vel dolor. Duis ut consequat ante, ut congue odio. Curabitur vitae ultrices nibh. Fusce varius elit leo, vitae gravida mi condimentum et. Mauris tellus lacus, pharetra vitae turpis tempor, auctor feugiat nibh. Nunc molestie mattis ultricies. Ut quam augue, ultrices sed odio sit amet, condimentum condimentum ante.\r\n\r\nSuspendisse dapibus ex nisl, ac ultrices nulla accumsan quis. Nam consectetur in diam id pellentesque. Ut feugiat eleifend dictum. Pellentesque pretium, lectus in consequat sollicitudin, diam tellus hendrerit mi, eget pretium lorem augue quis dui. Sed nec ante sem. Pellentesque at consectetur lorem. Quisque eleifend felis non orci gravida dignissim. Donec malesuada mi et elit maximus, nec vestibulum lacus scelerisque. Ut faucibus molestie eros, non venenatis ex luctus vitae. Vivamus cursus eros eu velit hendrerit auctor. In augue dui, ultrices sit amet magna sed, luctus facilisis augue. Donec id sapien magna.\r\n\r\nMorbi arcu nulla, venenatis at ex nec, dignissim commodo arcu. Ut dictum orci at dui vestibulum ultricies. Fusce vestibulum aliquam lacus vitae fringilla. Nulla sagittis a tortor nec tristique. Curabitur at lacus sed justo porta mollis. Etiam iaculis et sem eget ornare. Maecenas sed condimentum turpis, quis volutpat leo. Vivamus quis nulla est.', '2018-06-27 12:17:50'),
(4, 'Second Article', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque pharetra nec urna at convallis. Nulla interdum ornare quam, ut ultrices nibh fermentum ac. Vivamus vel magna sit amet erat interdum maximus eget vel dolor. Duis ut consequat ante, ut congue odio. Curabitur vitae ultrices nibh. Fusce varius elit leo, vitae gravida mi condimentum et. Mauris tellus lacus, pharetra vitae turpis tempor, auctor feugiat nibh. Nunc molestie mattis ultricies. Ut quam augue, ultrices sed odio sit amet, condimentum condimentum ante.\r\n\r\nSuspendisse dapibus ex nisl, ac ultrices nulla accumsan quis. Nam consectetur in diam id pellentesque. Ut feugiat eleifend dictum. Pellentesque pretium, lectus in consequat sollicitudin, diam tellus hendrerit mi, eget pretium lorem augue quis dui. Sed nec ante sem. Pellentesque at consectetur lorem. Quisque eleifend felis non orci gravida dignissim. Donec malesuada mi et elit maximus, nec vestibulum lacus scelerisque. Ut faucibus molestie eros, non venenatis ex luctus vitae. Vivamus cursus eros eu velit hendrerit auctor. In augue dui, ultrices sit amet magna sed, luctus facilisis augue. Donec id sapien magna.\r\n\r\nMorbi arcu nulla, venenatis at ex nec, dignissim commodo arcu. Ut dictum orci at dui vestibulum ultricies. Fusce vestibulum aliquam lacus vitae fringilla. Nulla sagittis a tortor nec tristique. Curabitur at lacus sed justo porta mollis. Etiam iaculis et sem eget ornare. Maecenas sed condimentum turpis, quis volutpat leo. Vivamus quis nulla est.', '2018-06-27 12:17:50');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(105) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `created_at`) VALUES
(1, 'solomond6@yahoo.com', 'password', '2018-06-28 05:07:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
