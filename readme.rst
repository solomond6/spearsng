###################
Platform Used
###################

- Php (Codeigniter Framework)
- Mysql (Database)
- Apache

*******************
Release Information
*******************

- Copy the Files system to your www directory or public_html folder (No need to install additional dependencies in this project)
- Create the database in mysql(you can use phpmyadmin), the name of the database can be "spearng"  and import the sql dump in the SQL Folder at the root directory
- Open the database config file in "application/config/database.php"
- Change the mysql connection credentials(username/password) to that of your mysql installation, also change the database name if you are not using "spearng".
- lunch in your browser : localhost/spearng

*******************
Task 1
*******************
- localhost/spearng
- Click on the Add Article to create new article
- Search of existing article using the search bar


*******************
Task 2
*******************
- Navigate the root directory from cmd
- run php index.php home createUser (this will prompt you to enter your email address and password)
- localhost/spearng/index.php/login (login form)


*******************
Technical Questions
*******************
1.	Creating USers via cmd, proper validation and hashing of password
2.	Break down the task in modules and each developer assigned to a module. Each developer will have to create a branch at the repo and for every push, a pull request have to be create in order to merge into the dev-master.
3.	Using verion control system
4.	Proper styling the autocomplete dropdown.